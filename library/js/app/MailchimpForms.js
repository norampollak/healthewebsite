/**
 * Author: CReich
 * Company: Rainbow Unicorn
 * Date: 08.07.2016
 * Created: 16:18
 **/
(function(window) {
  MailchimpForms.prototype.constructor = MailchimpForms;
  MailchimpForms.prototype = {
    val0: "",
    val1: ""
  };

  var ref, $, $forms;
  function MailchimpForms() {
    ref = this;
      $ = window.jQuery;
  }

  MailchimpForms.prototype.init = function() {
    /*
     * Mailchimp AJAX form submit VanillaJS
     * Vanilla JS
     * Author: Michiel Vandewalle
     * Github author: https://github.com/michiel-vandewalle
     * Github project: https://github.com/michiel-vandewalle/Mailchimp-AJAX-form-submit-vanillaJS
     */

    (function() {
      var form = document.getElementsByTagName("form");

      if (document.getElementsByTagName("form").length > 0) {
        document
          .getElementsByTagName("form")[0]
          .addEventListener("submit", function(e) {
            e.preventDefault();

            $(".sign-up-messages")
              .find(".inner")
              .html("Sending...");

            // Check for spam
            if (document.getElementById("js-validate-robot").value !== "") {
              return false;
            }

            // Get url for mailchimp
            var url = this.action.replace("/post?", "/post-json?");

            // Add form data to object
            var data = "";
            var inputs = this.querySelectorAll("#js-form-inputs input");
            for (var i = 0; i < inputs.length; i++) {
              data +=
                "&" +
                inputs[i].name +
                "=" +
                encodeURIComponent(inputs[i].value);
            }

            // Create & add post script to the DOM
            var script = document.createElement("script");
            script.src = url + data;
            document.body.appendChild(script);

            // Callback function
            var callback = "callback";
            window[callback] = function(data) {
              // Remove post script from the DOM
              delete window[callback];
              document.body.removeChild(script);

              var msg = data.msg;
              switch (data.msg) {
                case "0 - Please enter a value":
                  //case1

                  msg = "Step #1 — Write something in the field above please.";

                  break;
                case "0 - An email address must contain a single @":
                  //case2

                  msg = "Rethink your E-Mail address.";

                  break;

                  case "0 - An email address must contain a single @":
                      //case2

                      msg = "Rethink your E-Mail address.";

                      break;

                case "Thank you for subscribing!":
                  //case2

                  msg =
                    "Mummy was right: You are something special! Prepare to recieve very occasionally news from us.";

                  break;

                default:
                //default
              }

              // Display response message
              $(".sign-up-messages")
                .find(".inner")
                .html(msg);
            };
          });
      }
    })();
  };

  window.MailchimpForms = MailchimpForms;
})(window);
