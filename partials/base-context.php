<?php

$context = Timber::get_context();

/*
$context['options']['languageCode'] = ICL_LANGUAGE_CODE;
$languages = wpml_get_active_languages_filter('skip_missing=1');
$context['options']['languages'] = $languages;
*/

$context['isHome'] = is_front_page();
$context['front_page'] = new TimberPost(5);
$context['about_page'] = new TimberPost(30);
$context['fellowship_page'] = new TimberPost(51);

if($post->ID == $context['about_page']->ID){
    $context['isAboutPage'] = true;
} else $context['isAboutPage'] = false;
if($post->ID == $context['fellowship_page']->ID){
    $context['isFellowshipPage'] = true;
} else $context['isFellowshipPage'] = false;

if($context['isHome'] == false && $context['isAboutPage'] == false && $context['isFellowshipPage'] == false){
    //we are on a generic page
    $context['isGenericPage'] = true;
}

$post = new TimberPost();
$context['post'] = $post;

?>
