-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Erstellungszeit: 14. Okt 2020 um 09:10
-- Server-Version: 5.7.28
-- PHP-Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `thenewnew`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
CREATE TABLE IF NOT EXISTS `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
CREATE TABLE IF NOT EXISTS `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Daten für Tabelle `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-09-24 10:57:31', '2020-09-24 10:57:31', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
CREATE TABLE IF NOT EXISTS `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
CREATE TABLE IF NOT EXISTS `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`),
  KEY `autoload` (`autoload`)
) ENGINE=MyISAM AUTO_INCREMENT=365 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Daten für Tabelle `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://thenewnew.local', 'yes'),
(2, 'home', 'http://thenewnew.local', 'yes'),
(3, 'blogname', 'The New New', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'dev@rainbow-unicorn.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:8:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:33:\"duplicate-post/duplicate-post.php\";i:2;s:45:\"email-encoder-bundle/email-encoder-bundle.php\";i:3;s:59:\"force-regenerate-thumbnails/force-regenerate-thumbnails.php\";i:4;s:45:\"search-and-replace/inpsyde-search-replace.php\";i:5;s:25:\"timber-library/timber.php\";i:6;s:37:\"tinymce-advanced/tinymce-advanced.php\";i:7;s:41:\"wordpress-importer/wordpress-importer.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'thenewnew-theme', 'yes'),
(41, 'stylesheet', 'thenewnew-theme', 'yes'),
(42, 'comment_registration', '0', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '48748', 'yes'),
(47, 'uploads_use_yearmonth_folders', '1', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '1', 'yes'),
(50, 'default_link_category', '2', 'yes'),
(51, 'show_on_front', 'page', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '150', 'yes'),
(57, 'thumbnail_size_h', '150', 'yes'),
(58, 'thumbnail_crop', '1', 'yes'),
(59, 'medium_size_w', '300', 'yes'),
(60, 'medium_size_h', '300', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '1024', 'yes'),
(63, 'large_size_h', '1024', 'yes'),
(64, 'image_default_link_type', 'none', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '0', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '0', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:0:{}', 'yes'),
(76, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(77, 'widget_text', 'a:0:{}', 'yes'),
(78, 'widget_rss', 'a:0:{}', 'yes'),
(79, 'uninstall_plugins', 'a:0:{}', 'no'),
(80, 'timezone_string', '', 'yes'),
(81, 'page_for_posts', '0', 'yes'),
(82, 'page_on_front', '5', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '0', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1616497051', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'initial_db_version', '48748', 'yes'),
(96, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:62:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(97, 'fresh_site', '0', 'yes'),
(98, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:8:\"sidebar1\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(362, '_site_transient_timeout_theme_roots', '1602513985', 'no'),
(104, 'cron', 'a:9:{i:1602514652;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1602543452;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1602586652;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602586662;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602586663;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602590454;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1602763254;a:1:{s:16:\"wpseo_ryte_fetch\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1602845852;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'nonce_key', '[-zMm|*qFF2Y.Fi$10dWW>p5M$M/IDk,rc&Un_Cw#%OCF=/k0Q+3D~#A5P%IHd[/', 'no'),
(112, 'nonce_salt', '`Ct|PI4(:G^apLB]Y:u/x17bcUdNQ9kO/9H~Ze,}=}k! K*G-T{qv%i$H$f2HoK#', 'no'),
(113, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(125, 'auth_key', '&]O;t77O,BBPba_wu;VFp^{JB* 5UvfRde}tRQJ~<7wsXNe-)^;N!ffA-8<1,8=)', 'no'),
(117, 'recovery_keys', 'a:0:{}', 'yes'),
(118, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.5.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.5.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.5.1-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.5.1\";s:7:\"version\";s:5:\"5.5.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1602512183;s:15:\"version_checked\";s:5:\"5.5.1\";s:12:\"translations\";a:0:{}}', 'no'),
(121, 'theme_mods_twentytwenty', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1600948662;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(363, '_site_transient_theme_roots', 'a:1:{s:15:\"thenewnew-theme\";s:7:\"/themes\";}', 'no'),
(124, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1602512186;s:7:\"checked\";a:1:{s:15:\"thenewnew-theme\";s:3:\"1.0\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(248, 'theme_mods_thenewnew-theme', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:8:\"main-nav\";i:2;s:12:\"footer-links\";i:3;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(153, 'current_theme', 'The New New Theme', 'yes'),
(154, 'theme_mods_timber-bootstrap-gulp-master', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1601297277;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(155, 'theme_switched', '', 'yes'),
(126, 'auth_salt', '0*;Y(/ZVL[pUg%P NVcdbYFamRI]2Ec<<@tOaST@;0o^](l~mog=i< 2&]~<5:Z/', 'no'),
(127, 'logged_in_key', 'p,5@l*QzQ@D:)fRO!,Ws&&fa[9Z|^y/l~n{c^(-*4s0Jvj{6G)=UCRHL~dkxxg~%', 'no'),
(128, 'logged_in_salt', ',kd NE?jJY=,dGUTCVp* :V_eQW>VT(AfV5OI4),/o#82`f3yBc2zGWJ)tiT(TDM', 'no'),
(293, '_site_transient_timeout_php_check_7ddb89c02f1abf791c6717dc46cef1eb', '1602756168', 'no'),
(294, '_site_transient_php_check_7ddb89c02f1abf791c6717dc46cef1eb', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(296, '_site_transient_browser_ce4e9e986b0fbc713624d54b83c36283', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"85.0.4183.121\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(149, 'finished_updating_comment_type', '1', 'yes'),
(134, 'can_compress_scripts', '1', 'no'),
(309, '_site_transient_timeout_browser_c51375c8235cfb78e537fdf97dea9e10', '1602758790', 'no'),
(310, '_site_transient_browser_c51375c8235cfb78e537fdf97dea9e10', 'a:10:{s:4:\"name\";s:7:\"Firefox\";s:7:\"version\";s:4:\"80.0\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:32:\"https://www.mozilla.org/firefox/\";s:7:\"img_src\";s:44:\"http://s.w.org/images/browsers/firefox.png?1\";s:11:\"img_src_ssl\";s:45:\"https://s.w.org/images/browsers/firefox.png?1\";s:15:\"current_version\";s:2:\"56\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(319, 'options_newsletter_headline', 'Let’s <em>stay</em> in touch', 'no'),
(320, '_options_newsletter_headline', 'field_5f7ee56361005', 'no'),
(321, 'options_newsletter_button', 'Go', 'no'),
(322, '_options_newsletter_button', 'field_5f7ee5865f1ae', 'no'),
(323, 'options_apply_button', 'Apply', 'no'),
(324, '_options_apply_button', 'field_5f7ee86587b4c', 'no'),
(326, 'options_close_button', 'Close', 'no'),
(327, '_options_close_button', 'field_5f7f2727331c2', 'no'),
(345, 'options_to_top_button', 'Go up', 'no'),
(346, '_options_to_top_button', 'field_5f804f69628ec', 'no'),
(352, 'options_apply_url', 'mailto:dev@rainbow-unicorn.com', 'no'),
(353, '_options_apply_url', 'field_5f807a27e97d7', 'no'),
(354, 'options_newsletter_placeholder', 'Enter e-mail address', 'no'),
(355, '_options_newsletter_placeholder', 'field_5f8083e20b8e7', 'no'),
(182, 'rewrite_rules', 'a:130:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:14:\"custom_type/?$\";s:31:\"index.php?post_type=custom_type\";s:44:\"custom_type/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?post_type=custom_type&feed=$matches[1]\";s:39:\"custom_type/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?post_type=custom_type&feed=$matches[1]\";s:31:\"custom_type/page/([0-9]{1,})/?$\";s:49:\"index.php?post_type=custom_type&paged=$matches[1]\";s:52:\"custom-slug/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?custom_cat=$matches[1]&feed=$matches[2]\";s:47:\"custom-slug/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?custom_cat=$matches[1]&feed=$matches[2]\";s:28:\"custom-slug/([^/]+)/embed/?$\";s:43:\"index.php?custom_cat=$matches[1]&embed=true\";s:40:\"custom-slug/([^/]+)/page/?([0-9]{1,})/?$\";s:50:\"index.php?custom_cat=$matches[1]&paged=$matches[2]\";s:22:\"custom-slug/([^/]+)/?$\";s:32:\"index.php?custom_cat=$matches[1]\";s:51:\"custom_tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?custom_tag=$matches[1]&feed=$matches[2]\";s:46:\"custom_tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?custom_tag=$matches[1]&feed=$matches[2]\";s:27:\"custom_tag/([^/]+)/embed/?$\";s:43:\"index.php?custom_tag=$matches[1]&embed=true\";s:39:\"custom_tag/([^/]+)/page/?([0-9]{1,})/?$\";s:50:\"index.php?custom_tag=$matches[1]&paged=$matches[2]\";s:21:\"custom_tag/([^/]+)/?$\";s:32:\"index.php?custom_tag=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:39:\"custom_type/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:49:\"custom_type/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:69:\"custom_type/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"custom_type/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"custom_type/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:45:\"custom_type/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:28:\"custom_type/([^/]+)/embed/?$\";s:44:\"index.php?custom_type=$matches[1]&embed=true\";s:32:\"custom_type/([^/]+)/trackback/?$\";s:38:\"index.php?custom_type=$matches[1]&tb=1\";s:52:\"custom_type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?custom_type=$matches[1]&feed=$matches[2]\";s:47:\"custom_type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?custom_type=$matches[1]&feed=$matches[2]\";s:40:\"custom_type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?custom_type=$matches[1]&paged=$matches[2]\";s:47:\"custom_type/([^/]+)/comment-page-([0-9]{1,})/?$\";s:51:\"index.php?custom_type=$matches[1]&cpage=$matches[2]\";s:36:\"custom_type/([^/]+)(?:/([0-9]+))?/?$\";s:50:\"index.php?custom_type=$matches[1]&page=$matches[2]\";s:28:\"custom_type/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:38:\"custom_type/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:58:\"custom_type/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"custom_type/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"custom_type/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:34:\"custom_type/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=5&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(171, 'wpseo_ryte', 'a:2:{s:6:\"status\";i:-1;s:10:\"last_fetch\";i:1600948855;}', 'yes'),
(156, 'recently_activated', 'a:1:{s:24:\"wordpress-seo/wp-seo.php\";i:1600948858;}', 'yes'),
(256, 'category_children', 'a:0:{}', 'yes'),
(157, 'acf_version', '5.9.1', 'yes'),
(158, 'WP_Email_Encoder_Bundle_options', 'a:13:{s:7:\"protect\";i:1;s:10:\"filter_rss\";i:1;s:20:\"display_encoder_form\";i:1;s:10:\"powered_by\";i:1;s:13:\"protect_using\";s:15:\"with_javascript\";s:10:\"class_name\";s:9:\"mail-link\";s:15:\"protection_text\";s:17:\"*protected email*\";s:11:\"image_color\";s:5:\"0,0,0\";s:22:\"image_background_color\";s:5:\"0,0,0\";s:18:\"image_text_opacity\";s:1:\"0\";s:15:\"image_underline\";s:1:\"0\";s:24:\"image_background_opacity\";s:3:\"127\";s:15:\"image_font_size\";s:1:\"4\";}', 'yes'),
(159, 'email-encoder-bundle-version', '2.0.8', 'yes'),
(360, '_transient_timeout_acf_plugin_updates', '1602684985', 'no'),
(361, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:0:{}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.9.1\";}}', 'no'),
(162, 'tadv_settings', 'a:10:{s:7:\"options\";s:44:\"menubar,advlist,menubar_block,merge_toolbars\";s:7:\"plugins\";s:104:\"anchor,code,insertdatetime,nonbreaking,print,searchreplace,table,visualblocks,visualchars,advlist,wptadv\";s:9:\"toolbar_1\";s:106:\"formatselect,bold,italic,blockquote,bullist,numlist,alignleft,aligncenter,alignright,link,unlink,undo,redo\";s:9:\"toolbar_2\";s:103:\"fontselect,fontsizeselect,outdent,indent,pastetext,removeformat,charmap,wp_more,forecolor,table,wp_help\";s:9:\"toolbar_3\";s:0:\"\";s:9:\"toolbar_4\";s:0:\"\";s:21:\"toolbar_classic_block\";s:123:\"formatselect,bold,italic,blockquote,bullist,numlist,alignleft,aligncenter,alignright,link,forecolor,backcolor,table,wp_help\";s:13:\"toolbar_block\";s:67:\"core/code,core/image,core/strikethrough,tadv/mark,tadv/removeformat\";s:18:\"toolbar_block_side\";s:46:\"core/superscript,core/subscript,core/underline\";s:12:\"panels_block\";s:44:\"tadv/color-panel,tadv/background-color-panel\";}', 'yes'),
(163, 'tadv_admin_settings', 'a:1:{s:7:\"options\";s:86:\"classic_paragraph_block,table_resize_bars,table_grid,table_tab_navigation,table_advtab\";}', 'yes'),
(164, 'tadv_version', '5500', 'yes'),
(165, 'wpseo', 'a:30:{s:8:\"tracking\";b:0;s:22:\"license_server_version\";b:0;s:15:\"ms_defaults_set\";b:0;s:40:\"ignore_search_engines_discouraged_notice\";b:0;s:25:\"ignore_indexation_warning\";b:0;s:29:\"indexation_warning_hide_until\";b:0;s:18:\"indexation_started\";b:0;s:28:\"indexables_indexation_reason\";s:0:\"\";s:31:\"indexables_indexation_completed\";b:0;s:7:\"version\";s:4:\"14.9\";s:16:\"previous_version\";s:0:\"\";s:20:\"disableadvanced_meta\";b:1;s:30:\"enable_headless_rest_endpoints\";b:1;s:17:\"ryte_indexability\";b:1;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1600948854;s:13:\"myyoast-oauth\";b:0;}', 'yes'),
(166, 'yoast_migrations_free', 'a:1:{s:7:\"version\";s:4:\"14.9\";}', 'yes'),
(167, 'wpseo_titles', 'a:94:{s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:41:\"%%name%%, Author at %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:63:\"You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:35:\"Page not found %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:53:\"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:0;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:25:\"Error 404: Page not found\";s:29:\"breadcrumbs-display-blog-page\";b:1;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:12:\"Archives for\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:4:\"Home\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:16:\"You searched for\";s:15:\"breadcrumbs-sep\";s:7:\"&raquo;\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:11:\"person_logo\";s:0:\"\";s:14:\"person_logo_id\";i:0;s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:15:\"company_logo_id\";i:0;s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:7:\"company\";s:25:\"company_or_person_user_id\";b:0;s:17:\"stripcategorybase\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:23:\"post_types-post-maintax\";i:0;s:21:\"schema-page-type-post\";s:7:\"WebPage\";s:24:\"schema-article-type-post\";s:7:\"Article\";s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:23:\"post_types-page-maintax\";i:0;s:21:\"schema-page-type-page\";s:7:\"WebPage\";s:24:\"schema-article-type-page\";s:4:\"None\";s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:29:\"post_types-attachment-maintax\";i:0;s:27:\"schema-page-type-attachment\";s:7:\"WebPage\";s:30:\"schema-article-type-attachment\";s:4:\"None\";s:17:\"title-custom_type\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:20:\"metadesc-custom_type\";s:0:\"\";s:19:\"noindex-custom_type\";b:0;s:30:\"display-metabox-pt-custom_type\";b:1;s:30:\"post_types-custom_type-maintax\";i:0;s:28:\"schema-page-type-custom_type\";s:7:\"WebPage\";s:31:\"schema-article-type-custom_type\";s:4:\"None\";s:27:\"title-ptarchive-custom_type\";s:51:\"%%pt_plural%% Archive %%page%% %%sep%% %%sitename%%\";s:30:\"metadesc-ptarchive-custom_type\";s:0:\"\";s:29:\"bctitle-ptarchive-custom_type\";s:0:\"\";s:29:\"noindex-ptarchive-custom_type\";b:0;s:18:\"title-tax-category\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:1;s:23:\"noindex-tax-post_format\";b:1;s:20:\"title-tax-custom_cat\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:23:\"metadesc-tax-custom_cat\";s:0:\"\";s:30:\"display-metabox-tax-custom_cat\";b:1;s:22:\"noindex-tax-custom_cat\";b:0;s:28:\"taxonomy-custom_cat-ptparent\";i:0;s:20:\"title-tax-custom_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:23:\"metadesc-tax-custom_tag\";s:0:\"\";s:30:\"display-metabox-tax-custom_tag\";b:1;s:22:\"noindex-tax-custom_tag\";b:0;s:28:\"taxonomy-custom_tag-ptparent\";i:0;}', 'yes'),
(168, 'wpseo_social', 'a:19:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:19:\"og_default_image_id\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:21:\"og_frontpage_image_id\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:13:\"wikipedia_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(169, 'wpseo_flush_rewrite', '1', 'yes'),
(183, 'duplicate_post_copytitle', '1', 'yes'),
(184, 'duplicate_post_copydate', '0', 'yes'),
(185, 'duplicate_post_copystatus', '0', 'yes'),
(186, 'duplicate_post_copyslug', '0', 'yes'),
(187, 'duplicate_post_copyexcerpt', '1', 'yes'),
(188, 'duplicate_post_copycontent', '1', 'yes'),
(189, 'duplicate_post_copythumbnail', '1', 'yes'),
(190, 'duplicate_post_copytemplate', '1', 'yes'),
(191, 'duplicate_post_copyformat', '1', 'yes'),
(192, 'duplicate_post_copyauthor', '0', 'yes'),
(193, 'duplicate_post_copypassword', '0', 'yes'),
(194, 'duplicate_post_copyattachments', '0', 'yes'),
(195, 'duplicate_post_copychildren', '0', 'yes'),
(196, 'duplicate_post_copycomments', '0', 'yes'),
(197, 'duplicate_post_copymenuorder', '1', 'yes'),
(198, 'duplicate_post_taxonomies_blacklist', 'a:0:{}', 'yes'),
(199, 'duplicate_post_blacklist', '', 'yes'),
(200, 'duplicate_post_types_enabled', 'a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}', 'yes'),
(201, 'duplicate_post_show_row', '1', 'yes'),
(202, 'duplicate_post_show_adminbar', '1', 'yes'),
(203, 'duplicate_post_show_submitbox', '1', 'yes'),
(204, 'duplicate_post_show_bulkactions', '1', 'yes'),
(205, 'duplicate_post_show_original_column', '0', 'yes'),
(206, 'duplicate_post_show_original_in_post_states', '0', 'yes'),
(207, 'duplicate_post_show_original_meta_box', '0', 'yes'),
(211, 'duplicate_post_version', '3.2.6', 'yes'),
(222, '_transient_health-check-site-status-result', '{\"good\":9,\"recommended\":10,\"critical\":1}', 'yes'),
(241, 'acf_pro_license', 'YToyOntzOjM6ImtleSI7czo3MjoiYjNKa1pYSmZhV1E5TnpFNU9UTjhkSGx3WlQxa1pYWmxiRzl3WlhKOFpHRjBaVDB5TURFMkxUQXhMVEExSURFME9qUTJPakl6IjtzOjM6InVybCI7czoyMjoiaHR0cDovL3RoZW5ld25ldy5sb2NhbCI7fQ==', 'yes'),
(258, 'options_background_color', '#eeee22', 'no'),
(259, '_options_background_color', 'field_5f71e082402d6', 'no'),
(260, 'options_claim', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'no'),
(261, '_options_claim', 'field_5f71e0d67c8ca', 'no'),
(262, 'options_default_image', '14', 'no'),
(263, '_options_default_image', 'field_5f71e0f4a59a8', 'no'),
(267, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(295, '_site_transient_timeout_browser_ce4e9e986b0fbc713624d54b83c36283', '1602756169', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(364, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1602512187;s:7:\"checked\";a:12:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.9.1\";s:55:\"advanced-post-types-order/advanced-post-types-order.php\";s:7:\"4.3.8.3\";s:45:\"email-encoder-bundle/email-encoder-bundle.php\";s:5:\"2.0.8\";s:59:\"force-regenerate-thumbnails/force-regenerate-thumbnails.php\";s:5:\"2.0.6\";s:39:\"uk-cookie-consent/uk-cookie-consent.php\";s:6:\"2.3.15\";s:75:\"open-external-links-in-a-new-window/open-external-links-in-a-new-window.php\";s:3:\"1.4\";s:45:\"search-and-replace/inpsyde-search-replace.php\";s:5:\"3.2.1\";s:25:\"timber-library/timber.php\";s:6:\"1.18.0\";s:37:\"tinymce-advanced/tinymce-advanced.php\";s:5:\"5.5.0\";s:41:\"wordpress-importer/wordpress-importer.php\";s:3:\"0.7\";s:33:\"duplicate-post/duplicate-post.php\";s:5:\"3.2.6\";s:24:\"wordpress-seo/wp-seo.php\";s:4:\"14.9\";}s:8:\"response\";a:4:{s:45:\"email-encoder-bundle/email-encoder-bundle.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:34:\"w.org/plugins/email-encoder-bundle\";s:4:\"slug\";s:20:\"email-encoder-bundle\";s:6:\"plugin\";s:45:\"email-encoder-bundle/email-encoder-bundle.php\";s:11:\"new_version\";s:5:\"2.0.9\";s:3:\"url\";s:51:\"https://wordpress.org/plugins/email-encoder-bundle/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/email-encoder-bundle.2.0.9.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/email-encoder-bundle/assets/icon-256x256.png?rev=2056506\";s:2:\"1x\";s:73:\"https://ps.w.org/email-encoder-bundle/assets/icon-128x128.png?rev=2056506\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/email-encoder-bundle/assets/banner-1544x500.png?rev=2056506\";s:2:\"1x\";s:75:\"https://ps.w.org/email-encoder-bundle/assets/banner-772x250.png?rev=2056506\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";s:3:\"5.1\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:25:\"timber-library/timber.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/timber-library\";s:4:\"slug\";s:14:\"timber-library\";s:6:\"plugin\";s:25:\"timber-library/timber.php\";s:11:\"new_version\";s:6:\"1.18.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/timber-library/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/timber-library.1.18.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/timber-library/assets/icon-256x256.png?rev=1482054\";s:2:\"1x\";s:67:\"https://ps.w.org/timber-library/assets/icon-128x128.png?rev=1482055\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/timber-library/assets/banner-1544x500.jpg?rev=1481635\";s:2:\"1x\";s:69:\"https://ps.w.org/timber-library/assets/banner-772x250.jpg?rev=1481619\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:37:\"tinymce-advanced/tinymce-advanced.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:30:\"w.org/plugins/tinymce-advanced\";s:4:\"slug\";s:16:\"tinymce-advanced\";s:6:\"plugin\";s:37:\"tinymce-advanced/tinymce-advanced.php\";s:11:\"new_version\";s:5:\"5.5.1\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/tinymce-advanced/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/tinymce-advanced.5.5.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/tinymce-advanced/assets/icon-256x256.png?rev=971511\";s:2:\"1x\";s:68:\"https://ps.w.org/tinymce-advanced/assets/icon-128x128.png?rev=971511\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/tinymce-advanced/assets/banner-1544x500.png?rev=2390186\";s:2:\"1x\";s:71:\"https://ps.w.org/tinymce-advanced/assets/banner-772x250.png?rev=2390186\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"15.0\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.15.0.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=2363699\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}s:6:\"tested\";s:5:\"5.5.1\";s:12:\"requires_php\";s:6:\"5.6.20\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:6:{s:59:\"force-regenerate-thumbnails/force-regenerate-thumbnails.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:41:\"w.org/plugins/force-regenerate-thumbnails\";s:4:\"slug\";s:27:\"force-regenerate-thumbnails\";s:6:\"plugin\";s:59:\"force-regenerate-thumbnails/force-regenerate-thumbnails.php\";s:11:\"new_version\";s:5:\"2.0.6\";s:3:\"url\";s:58:\"https://wordpress.org/plugins/force-regenerate-thumbnails/\";s:7:\"package\";s:70:\"https://downloads.wordpress.org/plugin/force-regenerate-thumbnails.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:71:\"https://s.w.org/plugins/geopattern-icon/force-regenerate-thumbnails.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:39:\"uk-cookie-consent/uk-cookie-consent.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:31:\"w.org/plugins/uk-cookie-consent\";s:4:\"slug\";s:17:\"uk-cookie-consent\";s:6:\"plugin\";s:39:\"uk-cookie-consent/uk-cookie-consent.php\";s:11:\"new_version\";s:6:\"2.3.15\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/uk-cookie-consent/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/uk-cookie-consent.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/uk-cookie-consent/assets/icon-256x256.png?rev=1326394\";s:2:\"1x\";s:70:\"https://ps.w.org/uk-cookie-consent/assets/icon-128x128.png?rev=1326394\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/uk-cookie-consent/assets/banner-1544x500.jpg?rev=1326394\";s:2:\"1x\";s:72:\"https://ps.w.org/uk-cookie-consent/assets/banner-772x250.jpg?rev=1326394\";}s:11:\"banners_rtl\";a:0:{}}s:75:\"open-external-links-in-a-new-window/open-external-links-in-a-new-window.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:49:\"w.org/plugins/open-external-links-in-a-new-window\";s:4:\"slug\";s:35:\"open-external-links-in-a-new-window\";s:6:\"plugin\";s:75:\"open-external-links-in-a-new-window/open-external-links-in-a-new-window.php\";s:11:\"new_version\";s:3:\"1.4\";s:3:\"url\";s:66:\"https://wordpress.org/plugins/open-external-links-in-a-new-window/\";s:7:\"package\";s:82:\"https://downloads.wordpress.org/plugin/open-external-links-in-a-new-window.1.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:88:\"https://ps.w.org/open-external-links-in-a-new-window/assets/icon-256x256.png?rev=2145083\";s:2:\"1x\";s:88:\"https://ps.w.org/open-external-links-in-a-new-window/assets/icon-128x128.png?rev=2145083\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:89:\"https://ps.w.org/open-external-links-in-a-new-window/assets/banner-772x250.jpg?rev=547996\";}s:11:\"banners_rtl\";a:0:{}}s:45:\"search-and-replace/inpsyde-search-replace.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/search-and-replace\";s:4:\"slug\";s:18:\"search-and-replace\";s:6:\"plugin\";s:45:\"search-and-replace/inpsyde-search-replace.php\";s:11:\"new_version\";s:5:\"3.2.1\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/search-and-replace/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/search-and-replace.3.2.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/search-and-replace/assets/icon-256x256.png?rev=1776844\";s:2:\"1x\";s:71:\"https://ps.w.org/search-and-replace/assets/icon-128x128.png?rev=1776844\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:74:\"https://ps.w.org/search-and-replace/assets/banner-1544x500.png?rev=1776844\";s:2:\"1x\";s:73:\"https://ps.w.org/search-and-replace/assets/banner-772x250.png?rev=1776844\";}s:11:\"banners_rtl\";a:0:{}}s:41:\"wordpress-importer/wordpress-importer.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/wordpress-importer\";s:4:\"slug\";s:18:\"wordpress-importer\";s:6:\"plugin\";s:41:\"wordpress-importer/wordpress-importer.php\";s:11:\"new_version\";s:3:\"0.7\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/wordpress-importer/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/wordpress-importer.0.7.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:71:\"https://ps.w.org/wordpress-importer/assets/icon-256x256.png?rev=1908375\";s:2:\"1x\";s:63:\"https://ps.w.org/wordpress-importer/assets/icon.svg?rev=1908375\";s:3:\"svg\";s:63:\"https://ps.w.org/wordpress-importer/assets/icon.svg?rev=1908375\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-importer/assets/banner-772x250.png?rev=547654\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:5:\"3.2.6\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/duplicate-post.3.2.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=2336666\";s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=2336666\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/duplicate-post/assets/banner-1544x500.png?rev=2336666\";s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=2336666\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
CREATE TABLE IF NOT EXISTS `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=316 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Daten für Tabelle `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 2, '_edit_lock', '1601301004:1'),
(4, 5, '_edit_lock', '1602151545:1'),
(5, 5, '_wp_page_template', 'page-front.php'),
(107, 21, '_wp_trash_meta_status', 'publish'),
(7, 8, '_edit_last', '1'),
(8, 8, '_edit_lock', '1601297350:1'),
(9, 10, '_edit_lock', '1601297602:1'),
(33, 21, '_edit_last', '1'),
(34, 21, '_edit_lock', '1602151440:1'),
(19, 14, '_wp_attached_file', '2020/09/grumpy.jpg'),
(20, 14, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:225;s:6:\"height\";i:225;s:4:\"file\";s:18:\"2020/09/grumpy.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"grumpy-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"bones-thumb-600\";a:4:{s:4:\"file\";s:18:\"grumpy-225x150.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"bones-thumb-300\";a:4:{s:4:\"file\";s:18:\"grumpy-225x100.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"post-thumbnail\";a:4:{s:4:\"file\";s:18:\"grumpy-125x125.jpg\";s:5:\"width\";i:125;s:6:\"height\";i:125;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(29, 17, '_edit_lock', '1602257919:1'),
(24, 1, '_edit_lock', '1601371115:1'),
(23, 10, '_thumbnail_id', '14'),
(28, 17, '_edit_last', '1'),
(27, 1, '_thumbnail_id', '14'),
(30, 8, '_wp_trash_meta_status', 'publish'),
(31, 8, '_wp_trash_meta_time', '1601298868'),
(32, 8, '_wp_desired_post_slug', 'group_5f71d6eda170b'),
(35, 24, '_wp_attached_file', '2020/09/Mamman-Sani.jpg'),
(36, 24, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:888;s:6:\"height\";i:891;s:4:\"file\";s:23:\"2020/09/Mamman-Sani.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"Mamman-Sani-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"Mamman-Sani-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"Mamman-Sani-768x771.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:771;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"bones-thumb-600\";a:4:{s:4:\"file\";s:23:\"Mamman-Sani-600x150.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"bones-thumb-300\";a:4:{s:4:\"file\";s:23:\"Mamman-Sani-300x100.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"post-thumbnail\";a:4:{s:4:\"file\";s:23:\"Mamman-Sani-125x125.jpg\";s:5:\"width\";i:125;s:6:\"height\";i:125;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(37, 25, '_wp_attached_file', '2020/09/no_amp.jpg'),
(38, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:18:\"2020/09/no_amp.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"no_amp-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"no_amp-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"no_amp-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"no_amp-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"bones-thumb-600\";a:4:{s:4:\"file\";s:18:\"no_amp-600x150.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"bones-thumb-300\";a:4:{s:4:\"file\";s:18:\"no_amp-300x100.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"post-thumbnail\";a:4:{s:4:\"file\";s:18:\"no_amp-125x125.jpg\";s:5:\"width\";i:125;s:6:\"height\";i:125;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(39, 26, '_wp_attached_file', '2020/09/Phelimuncasi_2.jpg'),
(40, 26, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:26:\"2020/09/Phelimuncasi_2.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"Phelimuncasi_2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"Phelimuncasi_2-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"Phelimuncasi_2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"Phelimuncasi_2-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"bones-thumb-600\";a:4:{s:4:\"file\";s:26:\"Phelimuncasi_2-600x150.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"bones-thumb-300\";a:4:{s:4:\"file\";s:26:\"Phelimuncasi_2-300x100.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"post-thumbnail\";a:4:{s:4:\"file\";s:26:\"Phelimuncasi_2-125x125.jpg\";s:5:\"width\";i:125;s:6:\"height\";i:125;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(41, 5, '_edit_last', '1'),
(42, 5, 'images_0_image', '14'),
(43, 5, '_images_0_image', 'field_5f71e45f1b7c9'),
(44, 5, 'images_1_image', '24'),
(45, 5, '_images_1_image', 'field_5f71e45f1b7c9'),
(46, 5, 'images_2_image', '25'),
(47, 5, '_images_2_image', 'field_5f71e45f1b7c9'),
(48, 5, 'images_3_image', '26'),
(49, 5, '_images_3_image', 'field_5f71e45f1b7c9'),
(50, 5, 'images', '4'),
(51, 5, '_images', 'field_5f71e4421b7c8'),
(52, 27, 'images_0_image', '14'),
(53, 27, '_images_0_image', 'field_5f71e45f1b7c9'),
(54, 27, 'images_1_image', '24'),
(55, 27, '_images_1_image', 'field_5f71e45f1b7c9'),
(56, 27, 'images_2_image', '25'),
(57, 27, '_images_2_image', 'field_5f71e45f1b7c9'),
(58, 27, 'images_3_image', '26'),
(59, 27, '_images_3_image', 'field_5f71e45f1b7c9'),
(60, 27, 'images', '4'),
(61, 27, '_images', 'field_5f71e4421b7c8'),
(62, 5, 'images_0_linked', '1'),
(63, 5, '_images_0_linked', 'field_5f71eab7fd2e1'),
(64, 5, 'images_1_linked', '0'),
(65, 5, '_images_1_linked', 'field_5f71eab7fd2e1'),
(66, 5, 'images_2_linked', '0'),
(67, 5, '_images_2_linked', 'field_5f71eab7fd2e1'),
(68, 5, 'images_3_linked', '0'),
(69, 5, '_images_3_linked', 'field_5f71eab7fd2e1'),
(70, 29, 'images_0_image', '14'),
(71, 29, '_images_0_image', 'field_5f71e45f1b7c9'),
(72, 29, 'images_1_image', '24'),
(73, 29, '_images_1_image', 'field_5f71e45f1b7c9'),
(74, 29, 'images_2_image', '25'),
(75, 29, '_images_2_image', 'field_5f71e45f1b7c9'),
(76, 29, 'images_3_image', '26'),
(77, 29, '_images_3_image', 'field_5f71e45f1b7c9'),
(78, 29, 'images', '4'),
(79, 29, '_images', 'field_5f71e4421b7c8'),
(80, 29, 'images_0_linked', '1'),
(81, 29, '_images_0_linked', 'field_5f71eab7fd2e1'),
(82, 29, 'images_1_linked', '0'),
(83, 29, '_images_1_linked', 'field_5f71eab7fd2e1'),
(84, 29, 'images_2_linked', '0'),
(85, 29, '_images_2_linked', 'field_5f71eab7fd2e1'),
(86, 29, 'images_3_linked', '0'),
(87, 29, '_images_3_linked', 'field_5f71eab7fd2e1'),
(88, 30, '_edit_lock', '1602233187:1'),
(89, 32, '_menu_item_type', 'post_type'),
(90, 32, '_menu_item_menu_item_parent', '0'),
(91, 32, '_menu_item_object_id', '30'),
(92, 32, '_menu_item_object', 'page'),
(93, 32, '_menu_item_target', ''),
(94, 32, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(95, 32, '_menu_item_xfn', ''),
(96, 32, '_menu_item_url', ''),
(245, 65, '_jury_0_name', 'field_5f7ee5bddf856'),
(244, 65, 'jury_0_name', 'Edafe Onerhime'),
(243, 65, '_jury', 'field_5f7ee5abdf855'),
(242, 65, 'jury', '4'),
(241, 30, '_jury_headline', 'field_5f7f2aceebc39'),
(240, 30, 'jury_headline', 'Jury'),
(108, 21, '_wp_trash_meta_time', '1602151587'),
(109, 21, '_wp_desired_post_slug', 'group_5f71e411d437d'),
(110, 2, '_wp_trash_meta_status', 'publish'),
(111, 2, '_wp_trash_meta_time', '1602151597'),
(112, 2, '_wp_desired_post_slug', 'sample-page'),
(113, 41, '_edit_last', '1'),
(114, 41, '_edit_lock', '1602169421:1'),
(115, 30, '_wp_page_template', 'page-about.php'),
(116, 30, '_edit_last', '1'),
(117, 30, 'jury', '4'),
(118, 30, '_jury', 'field_5f7ee5abdf855'),
(119, 38, 'jury', ''),
(120, 38, '_jury', 'field_5f7ee5abdf855'),
(121, 30, 'jury_0_name', 'Edafe Onerhime'),
(122, 30, '_jury_0_name', 'field_5f7ee5bddf856'),
(123, 30, 'jury_0_info', 'Head of Data for Effective Development at the Department for International Development'),
(124, 30, '_jury_0_info', 'field_5f7ee5cadf857'),
(125, 30, 'jury_0_image', '26'),
(126, 30, '_jury_0_image', 'field_5f7ee5d9df858'),
(127, 30, 'jury_1_name', 'Fieke Jansen'),
(128, 30, '_jury_1_name', 'field_5f7ee5bddf856'),
(129, 30, 'jury_1_info', 'Data Justice Lab at Cardiff University, Mozilla Fellow'),
(130, 30, '_jury_1_info', 'field_5f7ee5cadf857'),
(131, 30, 'jury_1_image', '25'),
(132, 30, '_jury_1_image', 'field_5f7ee5d9df858'),
(133, 30, 'jury_2_name', 'Francesca Bria'),
(134, 30, '_jury_2_name', 'field_5f7ee5bddf856'),
(135, 30, 'jury_2_info', 'President of the Italian Innovation Fund'),
(136, 30, '_jury_2_info', 'field_5f7ee5cadf857'),
(137, 30, 'jury_2_image', '24'),
(138, 30, '_jury_2_image', 'field_5f7ee5d9df858'),
(139, 30, 'jury_3_name', 'Joana Varon'),
(140, 30, '_jury_3_name', 'field_5f7ee5bddf856'),
(141, 30, 'jury_3_info', 'Founder-Directress of Coding Rights'),
(142, 30, '_jury_3_info', 'field_5f7ee5cadf857'),
(143, 30, 'jury_3_image', '26'),
(144, 30, '_jury_3_image', 'field_5f7ee5d9df858'),
(145, 46, 'jury', '4'),
(146, 46, '_jury', 'field_5f7ee5abdf855'),
(147, 46, 'jury_0_name', 'Edafe Onerhime'),
(148, 46, '_jury_0_name', 'field_5f7ee5bddf856'),
(149, 46, 'jury_0_info', 'Head of Data for Effective Development at the Department for International Development'),
(150, 46, '_jury_0_info', 'field_5f7ee5cadf857'),
(151, 46, 'jury_0_image', ''),
(152, 46, '_jury_0_image', 'field_5f7ee5d9df858'),
(153, 46, 'jury_1_name', 'Fieke Jansen'),
(154, 46, '_jury_1_name', 'field_5f7ee5bddf856'),
(155, 46, 'jury_1_info', 'Data Justice Lab at Cardiff University, Mozilla Fellow'),
(156, 46, '_jury_1_info', 'field_5f7ee5cadf857'),
(157, 46, 'jury_1_image', ''),
(158, 46, '_jury_1_image', 'field_5f7ee5d9df858'),
(159, 46, 'jury_2_name', 'Francesca Bria'),
(160, 46, '_jury_2_name', 'field_5f7ee5bddf856'),
(161, 46, 'jury_2_info', 'President of the Italian Innovation Fund'),
(162, 46, '_jury_2_info', 'field_5f7ee5cadf857'),
(163, 46, 'jury_2_image', ''),
(164, 46, '_jury_2_image', 'field_5f7ee5d9df858'),
(165, 46, 'jury_3_name', 'Joana Varon'),
(166, 46, '_jury_3_name', 'field_5f7ee5bddf856'),
(167, 46, 'jury_3_info', 'Founder-Directress of Coding Rights'),
(168, 46, '_jury_3_info', 'field_5f7ee5cadf857'),
(169, 46, 'jury_3_image', ''),
(170, 46, '_jury_3_image', 'field_5f7ee5d9df858'),
(171, 47, '_edit_last', '1'),
(172, 47, '_edit_lock', '1602236278:1'),
(173, 51, '_edit_lock', '1602244303:1'),
(174, 51, '_wp_page_template', 'page-fellowship.php'),
(175, 51, '_edit_last', '1'),
(176, 51, 'faq', '3'),
(177, 51, '_faq', 'field_5f7ee7a78bc3e'),
(178, 52, 'faq', ''),
(179, 52, '_faq', 'field_5f7ee7a78bc3e'),
(180, 51, 'faq_0_headline', 'When is the deadline?'),
(181, 51, '_faq_0_headline', 'field_5f7ee7b58bc3f'),
(182, 51, 'faq_0_content', 'In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.'),
(183, 51, '_faq_0_content', 'field_5f7ee7be8bc40'),
(184, 51, 'faq_1_headline', 'How do I apply?'),
(185, 51, '_faq_1_headline', 'field_5f7ee7b58bc3f'),
(186, 51, 'faq_1_content', 'In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.'),
(187, 51, '_faq_1_content', 'field_5f7ee7be8bc40'),
(188, 51, 'faq_2_headline', 'Who is in Jury?'),
(189, 51, '_faq_2_headline', 'field_5f7ee7b58bc3f'),
(190, 51, 'faq_2_content', 'In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.'),
(191, 51, '_faq_2_content', 'field_5f7ee7be8bc40'),
(192, 53, 'faq', '3'),
(193, 53, '_faq', 'field_5f7ee7a78bc3e'),
(194, 53, 'faq_0_headline', 'When is the deadline?'),
(195, 53, '_faq_0_headline', 'field_5f7ee7b58bc3f'),
(196, 53, 'faq_0_content', 'In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.'),
(197, 53, '_faq_0_content', 'field_5f7ee7be8bc40'),
(198, 53, 'faq_1_headline', 'How do I apply?'),
(199, 53, '_faq_1_headline', 'field_5f7ee7b58bc3f'),
(200, 53, 'faq_1_content', 'In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.'),
(201, 53, '_faq_1_content', 'field_5f7ee7be8bc40'),
(202, 53, 'faq_2_headline', 'Who is in Jury?'),
(203, 53, '_faq_2_headline', 'field_5f7ee7b58bc3f'),
(204, 53, 'faq_2_content', 'In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.'),
(205, 53, '_faq_2_content', 'field_5f7ee7be8bc40'),
(206, 55, 'faq', '3'),
(207, 55, '_faq', 'field_5f7ee7a78bc3e'),
(208, 55, 'faq_0_headline', 'When is the deadline?'),
(209, 55, '_faq_0_headline', 'field_5f7ee7b58bc3f'),
(210, 55, 'faq_0_content', 'In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.'),
(211, 55, '_faq_0_content', 'field_5f7ee7be8bc40'),
(212, 55, 'faq_1_headline', 'How do I apply?'),
(213, 55, '_faq_1_headline', 'field_5f7ee7b58bc3f'),
(214, 55, 'faq_1_content', 'In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.'),
(215, 55, '_faq_1_content', 'field_5f7ee7be8bc40'),
(216, 55, 'faq_2_headline', 'Who is in Jury?'),
(217, 55, '_faq_2_headline', 'field_5f7ee7b58bc3f'),
(218, 55, 'faq_2_content', 'In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.'),
(219, 55, '_faq_2_content', 'field_5f7ee7be8bc40'),
(220, 57, '_edit_lock', '1602166733:1'),
(221, 59, '_edit_lock', '1602255243:1'),
(222, 61, '_menu_item_type', 'post_type'),
(223, 61, '_menu_item_menu_item_parent', '0'),
(224, 61, '_menu_item_object_id', '59'),
(225, 61, '_menu_item_object', 'page'),
(226, 61, '_menu_item_target', ''),
(227, 61, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(228, 61, '_menu_item_xfn', ''),
(229, 61, '_menu_item_url', ''),
(246, 65, 'jury_0_info', 'Head of Data for Effective Development at the Department for International Development'),
(231, 62, '_menu_item_type', 'post_type'),
(232, 62, '_menu_item_menu_item_parent', '0'),
(233, 62, '_menu_item_object_id', '57'),
(234, 62, '_menu_item_object', 'page'),
(235, 62, '_menu_item_target', ''),
(236, 62, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(237, 62, '_menu_item_xfn', ''),
(238, 62, '_menu_item_url', ''),
(247, 65, '_jury_0_info', 'field_5f7ee5cadf857'),
(248, 65, 'jury_0_image', ''),
(249, 65, '_jury_0_image', 'field_5f7ee5d9df858'),
(250, 65, 'jury_1_name', 'Fieke Jansen'),
(251, 65, '_jury_1_name', 'field_5f7ee5bddf856'),
(252, 65, 'jury_1_info', 'Data Justice Lab at Cardiff University, Mozilla Fellow'),
(253, 65, '_jury_1_info', 'field_5f7ee5cadf857'),
(254, 65, 'jury_1_image', ''),
(255, 65, '_jury_1_image', 'field_5f7ee5d9df858'),
(256, 65, 'jury_2_name', 'Francesca Bria'),
(257, 65, '_jury_2_name', 'field_5f7ee5bddf856'),
(258, 65, 'jury_2_info', 'President of the Italian Innovation Fund'),
(259, 65, '_jury_2_info', 'field_5f7ee5cadf857'),
(260, 65, 'jury_2_image', ''),
(261, 65, '_jury_2_image', 'field_5f7ee5d9df858'),
(262, 65, 'jury_3_name', 'Joana Varon'),
(263, 65, '_jury_3_name', 'field_5f7ee5bddf856'),
(264, 65, 'jury_3_info', 'Founder-Directress of Coding Rights'),
(265, 65, '_jury_3_info', 'field_5f7ee5cadf857'),
(266, 65, 'jury_3_image', ''),
(267, 65, '_jury_3_image', 'field_5f7ee5d9df858'),
(268, 65, 'jury_headline', 'Jury'),
(269, 65, '_jury_headline', 'field_5f7f2aceebc39'),
(270, 66, 'jury', '4'),
(271, 66, '_jury', 'field_5f7ee5abdf855'),
(272, 66, 'jury_0_name', 'Edafe Onerhime'),
(273, 66, '_jury_0_name', 'field_5f7ee5bddf856'),
(274, 66, 'jury_0_info', 'Head of Data for Effective Development at the Department for International Development'),
(275, 66, '_jury_0_info', 'field_5f7ee5cadf857'),
(276, 66, 'jury_0_image', '26'),
(277, 66, '_jury_0_image', 'field_5f7ee5d9df858'),
(278, 66, 'jury_1_name', 'Fieke Jansen'),
(279, 66, '_jury_1_name', 'field_5f7ee5bddf856'),
(280, 66, 'jury_1_info', 'Data Justice Lab at Cardiff University, Mozilla Fellow'),
(281, 66, '_jury_1_info', 'field_5f7ee5cadf857'),
(282, 66, 'jury_1_image', '25'),
(283, 66, '_jury_1_image', 'field_5f7ee5d9df858'),
(284, 66, 'jury_2_name', 'Francesca Bria'),
(285, 66, '_jury_2_name', 'field_5f7ee5bddf856'),
(286, 66, 'jury_2_info', 'President of the Italian Innovation Fund'),
(287, 66, '_jury_2_info', 'field_5f7ee5cadf857'),
(288, 66, 'jury_2_image', '24'),
(289, 66, '_jury_2_image', 'field_5f7ee5d9df858'),
(290, 66, 'jury_3_name', 'Joana Varon'),
(291, 66, '_jury_3_name', 'field_5f7ee5bddf856'),
(292, 66, 'jury_3_info', 'Founder-Directress of Coding Rights'),
(293, 66, '_jury_3_info', 'field_5f7ee5cadf857'),
(294, 66, 'jury_3_image', '26'),
(295, 66, '_jury_3_image', 'field_5f7ee5d9df858'),
(296, 66, 'jury_headline', 'Jury'),
(297, 66, '_jury_headline', 'field_5f7f2aceebc39'),
(298, 51, 'faq_headline', 'FAQ'),
(299, 51, '_faq_headline', 'field_5f802ff5bc59d'),
(300, 68, 'faq', '3'),
(301, 68, '_faq', 'field_5f7ee7a78bc3e'),
(302, 68, 'faq_0_headline', 'When is the deadline?'),
(303, 68, '_faq_0_headline', 'field_5f7ee7b58bc3f'),
(304, 68, 'faq_0_content', 'In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.'),
(305, 68, '_faq_0_content', 'field_5f7ee7be8bc40'),
(306, 68, 'faq_1_headline', 'How do I apply?'),
(307, 68, '_faq_1_headline', 'field_5f7ee7b58bc3f'),
(308, 68, 'faq_1_content', 'In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.'),
(309, 68, '_faq_1_content', 'field_5f7ee7be8bc40'),
(310, 68, 'faq_2_headline', 'Who is in Jury?'),
(311, 68, '_faq_2_headline', 'field_5f7ee7b58bc3f'),
(312, 68, 'faq_2_content', 'In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.'),
(313, 68, '_faq_2_content', 'field_5f7ee7be8bc40'),
(314, 68, 'faq_headline', 'FAQ'),
(315, 68, '_faq_headline', 'field_5f802ff5bc59d');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
CREATE TABLE IF NOT EXISTS `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Daten für Tabelle `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2020-09-24 10:57:31', '2020-09-24 10:57:31', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2020-09-28 12:55:58', '2020-09-28 12:55:58', '', 0, 'http://thenewnew.local/?p=1', 0, 'post', '', 1),
(2, 1, '2020-09-24 10:57:31', '2020-09-24 10:57:31', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://thenewnew.local/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'trash', 'closed', 'open', '', 'sample-page__trashed', '', '', '2020-10-08 10:06:37', '2020-10-08 10:06:37', '', 0, 'http://thenewnew.local/?page_id=2', 0, 'page', '', 0),
(3, 1, '2020-09-24 10:57:31', '2020-09-24 10:57:31', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://thenewnew.local.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2020-09-24 10:57:31', '2020-09-24 10:57:31', '', 0, 'http://thenewnew.local/?page_id=3', 0, 'page', '', 0),
(34, 1, '2020-10-08 10:02:50', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-10-08 10:02:50', '0000-00-00 00:00:00', '', 0, 'http://thenewnew.local/?p=34', 0, 'post', '', 0),
(5, 1, '2020-09-24 12:05:17', '2020-09-24 12:05:17', '<!-- wp:paragraph -->\n<p>Building inclusive<br>&amp; just digital futures</p>\n<!-- /wp:paragraph -->', 'Frontpage', '', 'publish', 'closed', 'closed', '', 'frontpage', '', '', '2020-10-08 10:07:40', '2020-10-08 10:07:40', '', 0, 'http://thenewnew.local/?page_id=5', 0, 'page', '', 0),
(6, 1, '2020-09-24 12:04:33', '2020-09-24 12:04:33', '', 'Frontpage', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2020-09-24 12:04:33', '2020-09-24 12:04:33', '', 5, 'http://thenewnew.local/2020/09/24/5-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2020-09-28 12:29:13', '2020-09-28 12:29:13', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Options', 'options', 'trash', 'closed', 'closed', '', 'group_5f71d6eda170b__trashed', '', '', '2020-09-28 13:14:28', '2020-09-28 13:14:28', '', 0, 'http://thenewnew.local/?post_type=acf-field-group&#038;p=8', 0, 'acf-field-group', '', 0),
(10, 1, '2020-09-28 12:51:57', '2020-09-28 12:51:57', '<!-- wp:paragraph -->\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->', '2nd Post', '', 'publish', 'open', 'open', '', '2nd-post', '', '', '2020-09-28 12:55:34', '2020-09-28 12:55:34', '', 0, 'http://thenewnew.local/?p=10', 0, 'post', '', 0),
(11, 1, '2020-09-28 12:51:57', '2020-09-28 12:51:57', '', '2nd Post', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2020-09-28 12:51:57', '2020-09-28 12:51:57', '', 10, 'http://thenewnew.local/2020/09/28/10-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2020-09-28 12:53:28', '2020-09-28 12:53:28', '<!-- wp:paragraph -->\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->', '2nd Post', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2020-09-28 12:53:28', '2020-09-28 12:53:28', '', 10, 'http://thenewnew.local/2020/09/28/10-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2020-09-28 12:55:30', '2020-09-28 12:55:30', '', 'grumpy', '', 'inherit', 'open', 'closed', '', 'grumpy', '', '', '2020-09-28 12:55:30', '2020-09-28 12:55:30', '', 10, 'http://thenewnew.local/wp-content/uploads/2020/09/grumpy.jpg', 0, 'attachment', 'image/jpeg', 0),
(15, 1, '2020-09-28 12:55:58', '2020-09-28 12:55:58', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-09-28 12:55:58', '2020-09-28 12:55:58', '', 1, 'http://thenewnew.local/2020/09/28/1-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2020-09-28 13:10:44', '2020-09-28 13:10:44', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:11:\"acf-options\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Options', 'options', 'publish', 'closed', 'closed', '', 'group_5f71e0662464c', '', '', '2020-10-09 15:38:22', '2020-10-09 15:38:22', '', 0, 'http://thenewnew.local/?post_type=acf-field-group&#038;p=17', 0, 'acf-field-group', '', 0),
(40, 1, '2020-10-08 10:10:27', '2020-10-08 10:10:27', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Newsletter button', 'newsletter_button', 'publish', 'closed', 'closed', '', 'field_5f7ee5865f1ae', '', '', '2020-10-08 10:10:27', '2020-10-08 10:10:27', '', 17, 'http://thenewnew.local/?post_type=acf-field&p=40', 1, 'acf-field', '', 0),
(41, 1, '2020-10-08 10:13:19', '2020-10-08 10:13:19', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"post_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"page-about.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'About Settings', 'about-settings', 'publish', 'closed', 'closed', '', 'group_5f7ee5a314ebe', '', '', '2020-10-08 15:06:04', '2020-10-08 15:06:04', '', 0, 'http://thenewnew.local/?post_type=acf-field-group&#038;p=41', 0, 'acf-field-group', '', 0),
(42, 1, '2020-10-08 10:13:19', '2020-10-08 10:13:19', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:10:\"Add Member\";}', 'Jury', 'jury', 'publish', 'closed', 'closed', '', 'field_5f7ee5abdf855', '', '', '2020-10-08 15:06:04', '2020-10-08 15:06:04', '', 41, 'http://thenewnew.local/?post_type=acf-field&#038;p=42', 1, 'acf-field', '', 0),
(43, 1, '2020-10-08 10:13:19', '2020-10-08 10:13:19', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Name', 'name', 'publish', 'closed', 'closed', '', 'field_5f7ee5bddf856', '', '', '2020-10-08 10:13:19', '2020-10-08 10:13:19', '', 42, 'http://thenewnew.local/?post_type=acf-field&p=43', 0, 'acf-field', '', 0),
(21, 1, '2020-09-28 13:26:06', '2020-09-28 13:26:06', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"post_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"page-front.php\";}}}s:8:\"position\";s:15:\"acf_after_title\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Frontpage Settings', 'frontpage-settings', 'trash', 'closed', 'closed', '', 'group_5f71e411d437d__trashed', '', '', '2020-10-08 10:06:27', '2020-10-08 10:06:27', '', 0, 'http://thenewnew.local/?post_type=acf-field-group&#038;p=21', 0, 'acf-field-group', '', 0),
(35, 1, '2020-10-08 10:06:37', '2020-10-08 10:06:37', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://thenewnew.local/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-10-08 10:06:37', '2020-10-08 10:06:37', '', 2, 'http://thenewnew.local/2020/10/08/2-revision-v1/', 0, 'revision', '', 0),
(24, 1, '2020-09-28 13:27:13', '2020-09-28 13:27:13', '', 'Mamman Sani', '', 'inherit', 'open', 'closed', '', 'mamman-sani', '', '', '2020-09-28 13:27:13', '2020-09-28 13:27:13', '', 5, 'http://thenewnew.local/wp-content/uploads/2020/09/Mamman-Sani.jpg', 0, 'attachment', 'image/jpeg', 0),
(25, 1, '2020-09-28 13:27:25', '2020-09-28 13:27:25', '', 'no_amp', '', 'inherit', 'open', 'closed', '', 'no_amp', '', '', '2020-09-28 13:27:25', '2020-09-28 13:27:25', '', 5, 'http://thenewnew.local/wp-content/uploads/2020/09/no_amp.jpg', 0, 'attachment', 'image/jpeg', 0),
(26, 1, '2020-09-28 13:27:36', '2020-09-28 13:27:36', '', 'Phelimuncasi_2', '', 'inherit', 'open', 'closed', '', 'phelimuncasi_2', '', '', '2020-09-28 13:27:36', '2020-09-28 13:27:36', '', 5, 'http://thenewnew.local/wp-content/uploads/2020/09/Phelimuncasi_2.jpg', 0, 'attachment', 'image/jpeg', 0),
(27, 1, '2020-09-28 13:27:42', '2020-09-28 13:27:42', '', 'Frontpage', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2020-09-28 13:27:42', '2020-09-28 13:27:42', '', 5, 'http://thenewnew.local/2020/09/28/5-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2020-10-08 10:07:40', '2020-10-08 10:07:40', '<!-- wp:paragraph -->\n<p>Building inclusive<br>&amp; just digital futures</p>\n<!-- /wp:paragraph -->', 'Frontpage', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2020-10-08 10:07:40', '2020-10-08 10:07:40', '', 5, 'http://thenewnew.local/2020/10/08/5-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2020-09-28 13:53:43', '2020-09-28 13:53:43', '', 'Frontpage', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2020-09-28 13:53:43', '2020-09-28 13:53:43', '', 5, 'http://thenewnew.local/2020/09/28/5-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2020-09-28 13:59:00', '2020-09-28 13:59:00', '<!-- wp:paragraph -->\n<p>In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>We want to approach all of this in a humble way: we do not set out to find the perfect answers, tools or approaches. We see this program as a forum in which discussions and experiments can begin. We’d much rather spend a year improving our ability to pose the right questions and digging deeper into certain topics than trying to implement half-baked approaches.</p>\n<!-- /wp:paragraph -->', 'About', '', 'publish', 'closed', 'closed', '', 'about', '', '', '2020-10-08 15:58:24', '2020-10-08 15:58:24', '', 0, 'http://thenewnew.local/?page_id=30', 0, 'page', '', 0),
(31, 1, '2020-09-28 13:59:00', '2020-09-28 13:59:00', '<!-- wp:paragraph -->\n<p>this is the about page text</p>\n<!-- /wp:paragraph -->', 'About Page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-09-28 13:59:00', '2020-09-28 13:59:00', '', 30, 'http://thenewnew.local/2020/09/28/30-revision-v1/', 0, 'revision', '', 0),
(32, 1, '2020-09-28 14:19:31', '2020-09-28 14:19:31', ' ', '', '', 'publish', 'closed', 'closed', '', '32', '', '', '2020-09-28 14:19:36', '2020-09-28 14:19:36', '', 0, 'http://thenewnew.local/?p=32', 1, 'nav_menu_item', '', 0),
(63, 1, '2020-10-08 14:50:21', '2020-10-08 14:50:21', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Close button', 'close_button', 'publish', 'closed', 'closed', '', 'field_5f7f2727331c2', '', '', '2020-10-09 15:38:22', '2020-10-09 15:38:22', '', 17, 'http://thenewnew.local/?post_type=acf-field&#038;p=63', 5, 'acf-field', '', 0),
(37, 1, '2020-10-08 10:08:31', '2020-10-08 10:08:31', '<!-- wp:paragraph -->\n<p>In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>We want to approach all of this in a humble way: we do not set out to find the perfect answers, tools or approaches. We see this program as a forum in which discussions and experiments can begin. We’d much rather spend a year improving our ability to pose the right questions and digging deeper into certain topics than trying to implement half-baked approaches.</p>\n<!-- /wp:paragraph -->', 'About Page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-10-08 10:08:31', '2020-10-08 10:08:31', '', 30, 'http://thenewnew.local/2020/10/08/30-revision-v1/', 0, 'revision', '', 0),
(38, 1, '2020-10-08 10:08:40', '2020-10-08 10:08:40', '<!-- wp:paragraph -->\n<p>In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>We want to approach all of this in a humble way: we do not set out to find the perfect answers, tools or approaches. We see this program as a forum in which discussions and experiments can begin. We’d much rather spend a year improving our ability to pose the right questions and digging deeper into certain topics than trying to implement half-baked approaches.</p>\n<!-- /wp:paragraph -->', 'About', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-10-08 10:08:40', '2020-10-08 10:08:40', '', 30, 'http://thenewnew.local/2020/10/08/30-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2020-10-08 10:10:10', '2020-10-08 10:10:10', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Newsletter headline', 'newsletter_headline', 'publish', 'closed', 'closed', '', 'field_5f7ee56361005', '', '', '2020-10-08 10:10:10', '2020-10-08 10:10:10', '', 17, 'http://thenewnew.local/?post_type=acf-field&p=39', 0, 'acf-field', '', 0),
(44, 1, '2020-10-08 10:13:19', '2020-10-08 10:13:19', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:0;s:5:\"delay\";i:0;}', 'Info', 'info', 'publish', 'closed', 'closed', '', 'field_5f7ee5cadf857', '', '', '2020-10-08 10:17:15', '2020-10-08 10:17:15', '', 42, 'http://thenewnew.local/?post_type=acf-field&#038;p=44', 2, 'acf-field', '', 0),
(45, 1, '2020-10-08 10:13:19', '2020-10-08 10:13:19', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_5f7ee5d9df858', '', '', '2020-10-08 10:17:15', '2020-10-08 10:17:15', '', 42, 'http://thenewnew.local/?post_type=acf-field&#038;p=45', 1, 'acf-field', '', 0),
(46, 1, '2020-10-08 10:16:22', '2020-10-08 10:16:22', '<!-- wp:paragraph -->\n<p>In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>We want to approach all of this in a humble way: we do not set out to find the perfect answers, tools or approaches. We see this program as a forum in which discussions and experiments can begin. We’d much rather spend a year improving our ability to pose the right questions and digging deeper into certain topics than trying to implement half-baked approaches.</p>\n<!-- /wp:paragraph -->', 'About', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-10-08 10:16:22', '2020-10-08 10:16:22', '', 30, 'http://thenewnew.local/2020/10/08/30-revision-v1/', 0, 'revision', '', 0),
(47, 1, '2020-10-08 10:20:18', '2020-10-08 10:20:18', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"post_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:19:\"page-fellowship.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Fellowship Settings', 'fellowship-settings', 'publish', 'closed', 'closed', '', 'group_5f7ee74903c8c', '', '', '2020-10-09 09:40:19', '2020-10-09 09:40:19', '', 0, 'http://thenewnew.local/?post_type=acf-field-group&#038;p=47', 0, 'acf-field-group', '', 0),
(48, 1, '2020-10-08 10:20:18', '2020-10-08 10:20:18', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'FAQ', 'faq', 'publish', 'closed', 'closed', '', 'field_5f7ee7a78bc3e', '', '', '2020-10-09 09:40:19', '2020-10-09 09:40:19', '', 47, 'http://thenewnew.local/?post_type=acf-field&#038;p=48', 1, 'acf-field', '', 0),
(49, 1, '2020-10-08 10:20:19', '2020-10-08 10:20:19', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Headline', 'headline', 'publish', 'closed', 'closed', '', 'field_5f7ee7b58bc3f', '', '', '2020-10-08 10:20:19', '2020-10-08 10:20:19', '', 48, 'http://thenewnew.local/?post_type=acf-field&p=49', 0, 'acf-field', '', 0),
(50, 1, '2020-10-08 10:20:19', '2020-10-08 10:20:19', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:0;s:5:\"delay\";i:0;}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_5f7ee7be8bc40', '', '', '2020-10-08 10:20:19', '2020-10-08 10:20:19', '', 48, 'http://thenewnew.local/?post_type=acf-field&p=50', 1, 'acf-field', '', 0),
(51, 1, '2020-10-08 10:20:38', '2020-10-08 10:20:38', '<!-- wp:paragraph -->\n<p>In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>We want to approach all of this in a humble way: we do not set out to find the perfect answers, tools or approaches. We see this program as a forum in which discussions and experiments can begin. We’d much rather spend a year improving our ability to pose the right questions and digging deeper into certain topics than trying to implement half-baked approaches.</p>\n<!-- /wp:paragraph -->', 'Fellowship', '', 'publish', 'closed', 'closed', '', 'fellowship', '', '', '2020-10-09 09:40:41', '2020-10-09 09:40:41', '', 0, 'http://thenewnew.local/?page_id=51', 0, 'page', '', 0),
(52, 1, '2020-10-08 10:20:38', '2020-10-08 10:20:38', '', 'Fellowship', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-08 10:20:38', '2020-10-08 10:20:38', '', 51, 'http://thenewnew.local/2020/10/08/51-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2020-10-08 10:21:52', '2020-10-08 10:21:52', '', 'Fellowship', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-08 10:21:52', '2020-10-08 10:21:52', '', 51, 'http://thenewnew.local/2020/10/08/51-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2020-10-08 10:22:04', '2020-10-08 10:22:04', '<!-- wp:paragraph -->\n<p>In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>We want to approach all of this in a humble way: we do not set out to find the perfect answers, tools or approaches. We see this program as a forum in which discussions and experiments can begin. We’d much rather spend a year improving our ability to pose the right questions and digging deeper into certain topics than trying to implement half-baked approaches.</p>\n<!-- /wp:paragraph -->', 'Fellowship', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-08 10:22:04', '2020-10-08 10:22:04', '', 51, 'http://thenewnew.local/2020/10/08/51-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2020-10-08 10:22:05', '2020-10-08 10:22:05', '<!-- wp:paragraph -->\n<p>In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>We want to approach all of this in a humble way: we do not set out to find the perfect answers, tools or approaches. We see this program as a forum in which discussions and experiments can begin. We’d much rather spend a year improving our ability to pose the right questions and digging deeper into certain topics than trying to implement half-baked approaches.</p>\n<!-- /wp:paragraph -->', 'Fellowship', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-08 10:22:05', '2020-10-08 10:22:05', '', 51, 'http://thenewnew.local/2020/10/08/51-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2020-10-08 10:22:57', '2020-10-08 10:22:57', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Apply button', 'apply_button', 'publish', 'closed', 'closed', '', 'field_5f7ee86587b4c', '', '', '2020-10-09 15:38:22', '2020-10-09 15:38:22', '', 17, 'http://thenewnew.local/?post_type=acf-field&#038;p=56', 3, 'acf-field', '', 0),
(57, 1, '2020-10-08 14:21:15', '2020-10-08 14:21:15', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\n<!-- /wp:paragraph -->', 'Imprint', '', 'publish', 'closed', 'closed', '', 'imprint', '', '', '2020-10-08 14:21:15', '2020-10-08 14:21:15', '', 0, 'http://thenewnew.local/?page_id=57', 0, 'page', '', 0),
(58, 1, '2020-10-08 14:21:15', '2020-10-08 14:21:15', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\n<!-- /wp:paragraph -->', 'Imprint', '', 'inherit', 'closed', 'closed', '', '57-revision-v1', '', '', '2020-10-08 14:21:15', '2020-10-08 14:21:15', '', 57, 'http://thenewnew.local/2020/10/08/57-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2020-10-08 14:21:32', '2020-10-08 14:21:32', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\n<!-- /wp:paragraph -->', 'Code of Conduct', '', 'publish', 'closed', 'closed', '', 'code-of-conduct', '', '', '2020-10-09 13:11:10', '2020-10-09 13:11:10', '', 0, 'http://thenewnew.local/?page_id=59', 0, 'page', '', 0),
(60, 1, '2020-10-08 14:21:32', '2020-10-08 14:21:32', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\n<!-- /wp:paragraph -->', 'Code of Conduct', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2020-10-08 14:21:32', '2020-10-08 14:21:32', '', 59, 'http://thenewnew.local/2020/10/08/59-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2020-10-08 14:21:53', '2020-10-08 14:21:53', ' ', '', '', 'publish', 'closed', 'closed', '', '61', '', '', '2020-10-08 14:21:53', '2020-10-08 14:21:53', '', 0, 'http://thenewnew.local/?p=61', 1, 'nav_menu_item', '', 0),
(62, 1, '2020-10-08 14:21:53', '2020-10-08 14:21:53', ' ', '', '', 'publish', 'closed', 'closed', '', '62', '', '', '2020-10-08 14:21:53', '2020-10-08 14:21:53', '', 0, 'http://thenewnew.local/?p=62', 2, 'nav_menu_item', '', 0),
(64, 1, '2020-10-08 15:06:04', '2020-10-08 15:06:04', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Jury Headline', 'jury_headline', 'publish', 'closed', 'closed', '', 'field_5f7f2aceebc39', '', '', '2020-10-08 15:06:04', '2020-10-08 15:06:04', '', 41, 'http://thenewnew.local/?post_type=acf-field&p=64', 0, 'acf-field', '', 0),
(65, 1, '2020-10-08 15:06:20', '2020-10-08 15:06:20', '<!-- wp:paragraph -->\n<p>In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>We want to approach all of this in a humble way: we do not set out to find the perfect answers, tools or approaches. We see this program as a forum in which discussions and experiments can begin. We’d much rather spend a year improving our ability to pose the right questions and digging deeper into certain topics than trying to implement half-baked approaches.</p>\n<!-- /wp:paragraph -->', 'About', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-10-08 15:06:20', '2020-10-08 15:06:20', '', 30, 'http://thenewnew.local/2020/10/08/30-revision-v1/', 0, 'revision', '', 0),
(66, 1, '2020-10-08 15:58:24', '2020-10-08 15:58:24', '<!-- wp:paragraph -->\n<p>In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>We want to approach all of this in a humble way: we do not set out to find the perfect answers, tools or approaches. We see this program as a forum in which discussions and experiments can begin. We’d much rather spend a year improving our ability to pose the right questions and digging deeper into certain topics than trying to implement half-baked approaches.</p>\n<!-- /wp:paragraph -->', 'About', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-10-08 15:58:24', '2020-10-08 15:58:24', '', 30, 'http://thenewnew.local/2020/10/08/30-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2020-10-09 09:40:19', '2020-10-09 09:40:19', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'FAQ Headline', 'faq_headline', 'publish', 'closed', 'closed', '', 'field_5f802ff5bc59d', '', '', '2020-10-09 09:40:19', '2020-10-09 09:40:19', '', 47, 'http://thenewnew.local/?post_type=acf-field&p=67', 0, 'acf-field', '', 0),
(68, 1, '2020-10-09 09:40:41', '2020-10-09 09:40:41', '<!-- wp:paragraph -->\n<p>In this joint program by the Bertelsmann Stiftung’s “Ethics of Algorithms” project and the Superrr Lab we aim to collectively explore what visionary, just and playful digital futures can look like. We want to create a space and community, where thinking about digital technologies allows us to reimagine current systems and practices. A space for questioning, discussion and exploration.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>We want to approach all of this in a humble way: we do not set out to find the perfect answers, tools or approaches. We see this program as a forum in which discussions and experiments can begin. We’d much rather spend a year improving our ability to pose the right questions and digging deeper into certain topics than trying to implement half-baked approaches.</p>\n<!-- /wp:paragraph -->', 'Fellowship', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-10-09 09:40:41', '2020-10-09 09:40:41', '', 51, 'http://thenewnew.local/2020/10/09/51-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2020-10-09 11:54:24', '2020-10-09 11:54:24', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'To Top Button', 'to_top_button', 'publish', 'closed', 'closed', '', 'field_5f804f69628ec', '', '', '2020-10-09 15:38:22', '2020-10-09 15:38:22', '', 17, 'http://thenewnew.local/?post_type=acf-field&#038;p=69', 6, 'acf-field', '', 0),
(70, 1, '2020-10-09 13:11:10', '2020-10-09 13:11:10', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\n<!-- /wp:paragraph -->', 'Code of Conduct', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2020-10-09 13:11:10', '2020-10-09 13:11:10', '', 59, 'http://thenewnew.local/2020/10/09/59-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2020-10-09 14:56:59', '2020-10-09 14:56:59', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Apply URL', 'apply_url', 'publish', 'closed', 'closed', '', 'field_5f807a27e97d7', '', '', '2020-10-09 15:38:22', '2020-10-09 15:38:22', '', 17, 'http://thenewnew.local/?post_type=acf-field&#038;p=71', 4, 'acf-field', '', 0),
(72, 1, '2020-10-09 15:38:22', '2020-10-09 15:38:22', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Newsletter placeholder', 'newsletter_placeholder', 'publish', 'closed', 'closed', '', 'field_5f8083e20b8e7', '', '', '2020-10-09 15:38:22', '2020-10-09 15:38:22', '', 17, 'http://thenewnew.local/?post_type=acf-field&p=72', 2, 'acf-field', '', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
CREATE TABLE IF NOT EXISTS `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
CREATE TABLE IF NOT EXISTS `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Daten für Tabelle `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'main-nav', 'main-nav', 0),
(3, 'footer-nav', 'footer-nav', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
CREATE TABLE IF NOT EXISTS `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Daten für Tabelle `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(10, 1, 0),
(32, 2, 0),
(61, 3, 0),
(62, 3, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Daten für Tabelle `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 2),
(2, 2, 'nav_menu', '', 0, 1),
(3, 3, 'nav_menu', '', 0, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
CREATE TABLE IF NOT EXISTS `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Daten für Tabelle `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'rainbowunicorn'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:2:{s:64:\"e89b255b0a79e149370c80c09ad7a7ab66518821882ca0b04d3cc6e02b924c69\";a:4:{s:10:\"expiration\";i:1602501454;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36\";s:5:\"login\";i:1601291854;}s:64:\"ed5dc32e4c3693e879a6f772b4f14847a1c0cf00bf17b0cfde4a4d21a9980da1\";a:4:{s:10:\"expiration\";i:1603363589;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:78:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0\";s:5:\"login\";i:1602153989;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '34'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(20, 1, 'wp_user-settings', 'libraryContent=browse'),
(21, 1, 'wp_user-settings-time', '1601297742'),
(22, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(23, 1, 'metaboxhidden_nav-menus', 'a:5:{i:0;s:25:\"add-post-type-custom_type\";i:1;s:12:\"add-post_tag\";i:2;s:15:\"add-post_format\";i:3;s:14:\"add-custom_cat\";i:4;s:14:\"add-custom_tag\";}'),
(24, 1, 'nav_menu_recently_edited', '3');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
CREATE TABLE IF NOT EXISTS `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Daten für Tabelle `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'rainbowunicorn', '$P$BIlxFtl61L.unkryrHFHGlwZZ0qdt7/', 'rainbowunicorn', 'dev@rainbow-unicorn.com', 'http://thenewnew.local', '2020-09-24 10:57:31', '', 0, 'rainbowunicorn');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_yoast_indexable`
--

DROP TABLE IF EXISTS `wp_yoast_indexable`;
CREATE TABLE IF NOT EXISTS `wp_yoast_indexable` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `permalink` longtext COLLATE utf8mb4_unicode_520_ci,
  `permalink_hash` varchar(40) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `object_id` int(11) UNSIGNED DEFAULT NULL,
  `object_type` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `object_sub_type` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `author_id` int(11) UNSIGNED DEFAULT NULL,
  `post_parent` int(11) UNSIGNED DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_520_ci,
  `description` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `breadcrumb_title` text COLLATE utf8mb4_unicode_520_ci,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_public` tinyint(1) DEFAULT NULL,
  `is_protected` tinyint(1) DEFAULT '0',
  `has_public_posts` tinyint(1) DEFAULT NULL,
  `number_of_pages` int(11) UNSIGNED DEFAULT NULL,
  `canonical` longtext COLLATE utf8mb4_unicode_520_ci,
  `primary_focus_keyword` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `primary_focus_keyword_score` int(3) DEFAULT NULL,
  `readability_score` int(3) DEFAULT NULL,
  `is_cornerstone` tinyint(1) DEFAULT '0',
  `is_robots_noindex` tinyint(1) DEFAULT '0',
  `is_robots_nofollow` tinyint(1) DEFAULT '0',
  `is_robots_noarchive` tinyint(1) DEFAULT '0',
  `is_robots_noimageindex` tinyint(1) DEFAULT '0',
  `is_robots_nosnippet` tinyint(1) DEFAULT '0',
  `twitter_title` text COLLATE utf8mb4_unicode_520_ci,
  `twitter_image` longtext COLLATE utf8mb4_unicode_520_ci,
  `twitter_description` longtext COLLATE utf8mb4_unicode_520_ci,
  `twitter_image_id` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `twitter_image_source` text COLLATE utf8mb4_unicode_520_ci,
  `open_graph_title` text COLLATE utf8mb4_unicode_520_ci,
  `open_graph_description` longtext COLLATE utf8mb4_unicode_520_ci,
  `open_graph_image` longtext COLLATE utf8mb4_unicode_520_ci,
  `open_graph_image_id` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `open_graph_image_source` text COLLATE utf8mb4_unicode_520_ci,
  `open_graph_image_meta` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `link_count` int(11) DEFAULT NULL,
  `incoming_link_count` int(11) DEFAULT NULL,
  `prominent_words_version` int(11) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `blog_id` bigint(20) NOT NULL DEFAULT '1',
  `language` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `region` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schema_page_type` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schema_article_type` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `has_ancestors` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `object_type_and_sub_type` (`object_type`,`object_sub_type`),
  KEY `object_id_and_type` (`object_id`,`object_type`),
  KEY `permalink_hash_and_object_type` (`permalink_hash`,`object_type`),
  KEY `subpages` (`post_parent`,`object_type`,`post_status`,`object_id`),
  KEY `prominent_words` (`prominent_words_version`,`object_type`,`object_sub_type`,`post_status`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Daten für Tabelle `wp_yoast_indexable`
--

INSERT INTO `wp_yoast_indexable` (`id`, `permalink`, `permalink_hash`, `object_id`, `object_type`, `object_sub_type`, `author_id`, `post_parent`, `title`, `description`, `breadcrumb_title`, `post_status`, `is_public`, `is_protected`, `has_public_posts`, `number_of_pages`, `canonical`, `primary_focus_keyword`, `primary_focus_keyword_score`, `readability_score`, `is_cornerstone`, `is_robots_noindex`, `is_robots_nofollow`, `is_robots_noarchive`, `is_robots_noimageindex`, `is_robots_nosnippet`, `twitter_title`, `twitter_image`, `twitter_description`, `twitter_image_id`, `twitter_image_source`, `open_graph_title`, `open_graph_description`, `open_graph_image`, `open_graph_image_id`, `open_graph_image_source`, `open_graph_image_meta`, `link_count`, `incoming_link_count`, `prominent_words_version`, `created_at`, `updated_at`, `blog_id`, `language`, `region`, `schema_page_type`, `schema_article_type`, `has_ancestors`) VALUES
(1, 'http://thenewnew.local/author/rainbowunicorn/', '45:f244cab242dd8dc3193ab34b9bf926d0', 1, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'https://2.gravatar.com/avatar/5cdb1b12bfcdad8635a5a1b13d231bd4?s=500&d=mm&r=g', NULL, NULL, 'gravatar-image', NULL, NULL, 'https://2.gravatar.com/avatar/5cdb1b12bfcdad8635a5a1b13d231bd4?s=500&d=mm&r=g', NULL, 'gravatar-image', NULL, NULL, NULL, NULL, '2020-09-24 12:00:56', '2020-09-24 10:00:56', 1, NULL, NULL, NULL, NULL, 0),
(2, 'http://thenewnew.local/?page_id=3', '33:bab4f67c1e69642568f9e563a14e8f1b', 3, 'post', 'page', 1, 0, NULL, NULL, 'Privacy Policy', 'draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-24 12:00:56', '2020-09-24 10:00:56', 1, NULL, NULL, NULL, NULL, 0),
(3, 'http://thenewnew.local/sample-page/', '35:cb45681a876ade0e99373de77d9fb86e', 2, 'post', 'page', 1, 0, NULL, NULL, 'Sample Page', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-24 12:00:56', '2020-09-24 10:00:56', 1, NULL, NULL, NULL, NULL, 0),
(4, 'http://thenewnew.local/?p=4', '27:c2b6fd341324ab6ac48a5b22a7c77245', 4, 'post', 'post', 1, 0, NULL, NULL, 'Auto Draft', 'auto-draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-24 12:00:56', '2020-09-24 10:00:56', 1, NULL, NULL, NULL, NULL, 0),
(5, 'http://thenewnew.local/2020/09/24/hello-world/', '46:c52b6ec32c2af696c095223a7db0ed0c', 1, 'post', 'post', 1, 0, NULL, NULL, 'Hello world!', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-24 12:00:56', '2020-09-24 10:00:56', 1, NULL, NULL, NULL, NULL, 0),
(6, 'http://thenewnew.local/category/uncategorized/', '46:a1c4c11eb55ba022945f5888eb8ceaf9', 1, 'term', 'category', NULL, NULL, NULL, NULL, 'Uncategorized', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-09-24 12:00:56', '2020-09-24 10:00:56', 1, NULL, NULL, NULL, NULL, 0),
(7, NULL, NULL, NULL, 'system-page', '404', NULL, NULL, 'Page not found %%sep%% %%sitename%%', NULL, 'Error 404: Page not found', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-24 12:00:56', '2020-09-24 10:00:56', 1, NULL, NULL, NULL, NULL, 0),
(8, NULL, NULL, NULL, 'system-page', 'search-result', NULL, NULL, 'You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-24 12:00:56', '2020-09-24 10:00:56', 1, NULL, NULL, NULL, NULL, 0),
(9, NULL, NULL, NULL, 'date-archive', NULL, NULL, NULL, '%%date%% %%page%% %%sep%% %%sitename%%', '', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-24 12:00:56', '2020-09-24 10:00:56', 1, NULL, NULL, NULL, NULL, 0),
(10, 'http://thenewnew.local/', '23:91dd7394d560435469e66cd3989c6b4a', NULL, 'home-page', NULL, NULL, NULL, '%%sitename%% %%page%% %%sep%% %%sitedesc%%', 'Just another WordPress site', 'Home', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '2020-09-24 12:00:56', '2020-09-24 10:00:56', 1, NULL, NULL, NULL, NULL, 0),
(11, 'http://thenewnew.local/custom_type/', '35:7070994fac8ba28f67b37283829ce0eb', NULL, 'post-type-archive', 'custom_type', NULL, NULL, '%%pt_plural%% Archive %%page%% %%sep%% %%sitename%%', '', 'Custom Types', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-24 12:00:56', '2020-09-24 10:00:56', 1, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_yoast_indexable_hierarchy`
--

DROP TABLE IF EXISTS `wp_yoast_indexable_hierarchy`;
CREATE TABLE IF NOT EXISTS `wp_yoast_indexable_hierarchy` (
  `indexable_id` int(11) UNSIGNED NOT NULL,
  `ancestor_id` int(11) UNSIGNED NOT NULL,
  `depth` int(11) UNSIGNED DEFAULT NULL,
  `blog_id` bigint(20) NOT NULL DEFAULT '1',
  PRIMARY KEY (`indexable_id`,`ancestor_id`),
  KEY `indexable_id` (`indexable_id`),
  KEY `ancestor_id` (`ancestor_id`),
  KEY `depth` (`depth`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_yoast_migrations`
--

DROP TABLE IF EXISTS `wp_yoast_migrations`;
CREATE TABLE IF NOT EXISTS `wp_yoast_migrations` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `version` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wp_yoast_migrations_version` (`version`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Daten für Tabelle `wp_yoast_migrations`
--

INSERT INTO `wp_yoast_migrations` (`id`, `version`) VALUES
(1, '20171228151840'),
(2, '20171228151841'),
(3, '20190529075038'),
(4, '20191011111109'),
(5, '20200408101900'),
(6, '20200420073606'),
(7, '20200428123747'),
(8, '20200428194858'),
(9, '20200429105310'),
(10, '20200430075614'),
(11, '20200430150130'),
(12, '20200507054848'),
(13, '20200513133401'),
(14, '20200609154515'),
(15, '20200616130143'),
(16, '20200617122511'),
(17, '20200702141921'),
(18, '20200728095334');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_yoast_primary_term`
--

DROP TABLE IF EXISTS `wp_yoast_primary_term`;
CREATE TABLE IF NOT EXISTS `wp_yoast_primary_term` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` int(11) UNSIGNED NOT NULL,
  `term_id` int(11) UNSIGNED NOT NULL,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `blog_id` bigint(20) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `post_taxonomy` (`post_id`,`taxonomy`),
  KEY `post_term` (`post_id`,`term_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wp_yoast_seo_links`
--

DROP TABLE IF EXISTS `wp_yoast_seo_links`;
CREATE TABLE IF NOT EXISTS `wp_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `target_post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` varchar(8) DEFAULT NULL,
  `indexable_id` int(11) UNSIGNED DEFAULT NULL,
  `target_indexable_id` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `width` int(11) UNSIGNED DEFAULT NULL,
  `size` int(11) UNSIGNED DEFAULT NULL,
  `language` varchar(32) DEFAULT NULL,
  `region` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `link_direction` (`post_id`,`type`),
  KEY `indexable_link_direction` (`indexable_id`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `wp_yoast_seo_links`
--

INSERT INTO `wp_yoast_seo_links` (`id`, `url`, `post_id`, `target_post_id`, `type`, `indexable_id`, `target_indexable_id`, `height`, `width`, `size`, `language`, `region`) VALUES
(1, 'http://thenewnew.local/wp-admin/', 2, NULL, 'internal', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
